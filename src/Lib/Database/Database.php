<?php
namespace Sannomiya\Database;

abstract class Database {
    public const FETCH_TYPE_NUM = 2;
    public const FETCH_TYPE_ASSOC = 1;
    public const DATABASE_TYPE_PDO = 1;

    protected $fetch_type = self::FETCH_TYPE_ASSOC;

    public string $uniqueId = "";
    public const ERROR_RESTRICT = 23000;
    protected ?string $lastErrorMessage = null;
    protected ?string $lastErrorCode = null;
	protected string $schema;

    public static array $databaseConfig = [];
    public static ?string $databaseDefault = null;

    static public function addDatabase($name, $dns, $user, $password, $options=[], $type=Database::DATABASE_TYPE_PDO) {
        static::$databaseConfig[$name] = [
            "database_type" => $type,
            "dsn" => $dns,
            "user" => $user,
            "password" => $password,
            "options" => $options
        ];
    }

    static public function setDefaultDatabaseName(?string $databaseName, bool $keepInSession=true) {
        static::$databaseDefault = $databaseName;
        if ($keepInSession) {
            $_SESSION['DATABASE_DEFAULT_NAME'] = $databaseName;
        }else{
            unset($_SESSION['DATABASE_DEFAULT_NAME']);
        }
    }

    static public function getDefaultDatabaseName() {
        if (isset($_SESSION) && isset($_SESSION['DATABASE_DEFAULT_NAME'])) {
            return $_SESSION['DATABASE_DEFAULT_NAME'];
        }
        return static::$databaseDefault;
    }

    static public function getInstance($databaseName=null, $newInstance = false) : Database {
        global $database_config, $database_default;
        if (isset($database_config)) {
            static::$databaseConfig = $database_config;
        }
        if (isset($database_default)) {
            static::$databaseDefault = $database_default;
        }

        if (!isset($databaseName)){
            $databaseName = self::getDefaultDatabaseName();
        }

        if (!isset($databaseName) || $databaseName == ''){
            $databaseName = $_SERVER['HTTP_HOST'];
        }

        if (!isset(static::$databaseConfig[$databaseName])) {
            exit("Database $databaseName not found!");
        }

        $data_info = static::$databaseConfig[$databaseName];
        $request_database_name = "_database_$databaseName";

        // Get instance from REQUEST
        if (!$newInstance && isset($_REQUEST[$request_database_name])){
            return $_REQUEST[$request_database_name];
        }
        switch ($data_info["database_type"]) {
            case self::DATABASE_TYPE_PDO:
                $ret = new PDODatabase($data_info);
                break;
            default:
                exit("Database type {$data_info["database_type"]} not implemented.");
        }

        // Save instance in REQUEST
        if (!$newInstance) {
            $_REQUEST[$request_database_name] = $ret;
        }

        return $ret;
    }

    abstract public  function begin();
    abstract public  function commit();
    abstract public  function rollback();
    abstract public  function close();

    /**
     * Get first field in first record return by $query
     * @param string $query
     * @param array $params
     * @return mixed
     */
    abstract public  function get(string $query, $params=[]);
    abstract public  function getRecord($query, $params=[]);
    abstract public  function getRecordSet($query, &$total, $start=0, $page_size=-1, $params=[]): array;
    abstract public  function execute($query, $params=[]);
    abstract public  function insertedId($name = null): ?int;
    abstract public  function getTotal($query, $params = []): int;

    /**
     * Prepare SQL statement for fetch
     * @param string $query
     * @param array $params
     * @return mixed
     */
    abstract public  function prepare(string $query, $params=[]);

    /**
     * Use this to get record from statement that prepared
     * @return mixed
     */
    abstract public  function fetch();

    abstract public  function updateSequence($table): void;

    abstract public  function getConn();

    abstract public function getLocking(int $time = 0, ?string $pattern = null): array;

    abstract public function isLocking(int $time = 0, ?string $pattern = null): bool;

    abstract public function killLocking(int $time = 0, ?string $pattern = null): int;
    /**
     * @return int
     */
    public function getFetchType(): int
    {
        return $this->fetch_type;
    }

    /**
     * @param int $fetch_type
     */
    public function setFetchType(int $fetch_type)
    {
        $this->fetch_type = $fetch_type;
    }

    public function getMap($query, $params=[]): array
    {
        $fetch_type = $this->getFetchType();
        $this->setFetchType(self::FETCH_TYPE_NUM);
        $rs = $this->getRecordSet( $query, $t, 0, -1, $params );
        $this->setFetchType($fetch_type);
        $ret = [];
        if (is_array ( $rs )) {
            foreach ( $rs as $rec ) {
                $ret [$rec [0]] = $rec [1];
            }
        }
        return $ret;
    }

    public function getMapOfMap($query, $keys, $params=[]): array
    {
        $fetch_type = $this->getFetchType();
        $this->setFetchType(self::FETCH_TYPE_ASSOC);
        $rs = $this->getRecordSet( $query, $t, 0, -1, $params );
        $this->setFetchType($fetch_type);
        if (!is_array($keys)){
            $keys = explode(',', $keys);
        }
        $ret = [];
        if (is_array ( $rs )) {
            foreach ( $rs as $rec ) {
                $map_keys = [];
                foreach ($keys as $key) {
                    $map_keys[] = @$rec[$key];
                }
                $ret [implode('_', $map_keys)] = $rec;
            }
        }
        return $ret;
    }

    public function getList($query, $params=[]): array
    {
        $fetch_type = $this->getFetchType();
        $this->setFetchType(self::FETCH_TYPE_NUM);
        $rs = $this->getRecordSet( $query, $t, 0, -1, $params );
        $this->setFetchType($fetch_type);
        $ret = [];
        foreach ($rs as $rec) {
            $ret[] = $rec[0];
        }
        return $ret;
    }

    public function getListOfList($query, $params=[], $max=null): array
    {
        if (!is_numeric($max)) {
            $max = -1;
        }
        $fetch_type = $this->getFetchType();
        $this->setFetchType(self::FETCH_TYPE_NUM);
        $rs = $this->getRecordSet( $query, $t, 0, $max, $params );
        $this->setFetchType($fetch_type);
        return  $rs;
    }

    private function getRecordValues($rec, $fields): array
    {
        $ret = [];
        foreach ($fields as $field) {
            $ret[] = $rec[$field];
        }
        return $ret;
    }

    public function checkUnique(array $rs, string $table, $checkFields, ?string &$errorValues, $where = null, $keyFields='id', $update = true): bool {
        if (!is_array($checkFields)) {
            $checkFields = explode(",", $checkFields);
        }
        if (!is_array($keyFields)) {
            $keyFields = explode(",", $keyFields);
        }
        $selectField = $checkFields[0];
        $whereFields = [];
        foreach ($checkFields as $field){
            $whereFields[] = "$field=?";
        }
        $whereFields = implode(" AND ", $whereFields);

        $values = [];
        if (isset($where)){
            $where = "and $where";
        }

        foreach ($rs as $rec) {
            $ok = true;
            $params = $this->getRecordValues($rec,$checkFields);
            $value = implode(',', $params);
            if (in_array($value, $values)) {
                $ok = false;
            }else if (isset($value) && $value!=''){
                $values[] = $value;
                // Check key
                $checkKeys = [];
                if ($update) {
                    foreach ($keyFields as $key) {
                        if (isset($rec[$key])){
                            if (is_numeric($rec[$key])) {
                                $checkKeys[] = "$key <> {$rec[$key]}";
                            }else{
                                $checkKeys[] = "$key <> '{$rec[$key]}'";
                            }

                        }
                    }
                }
                if (count($checkKeys) == count($keyFields)) {
                    $whereKey = ' AND ' . '(' . implode(' OR ', $checkKeys) . ')';
                }else{
                    $whereKey = '';
                }
                $check = $this->get("select "."$selectField from $table where $whereFields $whereKey $where", $params);

                if (isset($check)){
                    $ok = false;
                }
            }

            if (!$ok){
                $errorValues = $value;
                return false;
            }
        }
        return true;
    }

    /**
     * @return string|null
     */
    public function getLastErrorCode(): ?string
    {
        return $this->lastErrorCode;
    }

    /**
     * @return string
     */
    public function getSchema(): string
    {
        return $this->schema;
    }


}



