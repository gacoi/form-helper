<?php

namespace Sannomiya\Form;

use DateTime;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\Exception\Exception;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;

class WordReporter extends Bag
{
    private ?string $word_template = null;

    private array $main_data = [];
    private array $detail_data = [];

    private array $block_data = [];

    private string $file_name = "report.docx";

    private ?PhpWord $word = null;

    private ?TemplateProcessor $processor = null;

    public function __construct($word_template)
    {
        $this->set_word_template($word_template);
    }

    public function set_output_file_name($value){
        $this->file_name = $value;
    }

    public function get_output_file_name(): ?string
    {
        return $this->file_name;
    }

    /**
     * @param array $data ex: ['field1'=>1, 'field2'=>2]
     * @return void
     */
    public function set_main_data(array $data) {
        $this->main_data[] = $data;
    }


    /**
     * Same as set_table_data
     * @param string $key
     * @param array $data ex: [['field1'=>1, 'field2'=>2], ['field1'=>3, 'field3'=>4]]
     * @return void
     */
    public function set_detail_data(string $key, array $data ){
        $this->detail_data[$key] = $data;
    }

    /**
     * @param string $key
     * @param array $data ex: [['field1'=>1, 'field2'=>2], ['field1'=>3, 'field3'=>4]]
     * @return void
     */
    public function set_table_data(string $key, array $data ){
        $this->detail_data[$key] = $data;
    }

    /**
     * @param string $blockName
     * @param array $data ex: [['field1'=>1, 'field2'=>2], ['field1'=>3, 'field3'=>4]]
     * @return void
     */
    public function set_block_data(string $blockName, array $data ){
        $this->block_data[$blockName] = $data;
    }

    /**
     * @return PhpWord
     */
    public function get_word(): ?PhpWord
    {
        return $this->word;
    }

    public function set_word_template($template){
        $this->word_template = $template;
    }

    /**
     * @throws CopyFileException
     * @throws CreateTemporaryFileException
     */
    public function create_word()
    {
        $this->processor = new TemplateProcessor($this->word_template);
        $variables = $this->processor->getVariables();
        $map = [];
        foreach ($variables as $value) {
            $i = strpos($value, ':') ;
            if ($i !== false) {
                $fieldName = substr($value, 0, $i);
                $map[$fieldName] = substr($value, $i);;
            }else{
                $map[$value] = '';
            }
        }

        foreach ($this->detail_data as $key=>$values) {
            $images = $this->get_images($values);
            $this->processor->cloneRowAndSetValues($key, $values);
            foreach ($images as $fieldName => $imagePath) {
                if (isset($imagePath)) {
                    $this->processor->setImageValue($fieldName, $imagePath);
                }else{
                    $mapFieldName = preg_replace('/#[0-9]+$/', '', $fieldName);
                    $this->processor->setValue($fieldName . $map[$mapFieldName] , null);
                }
            }
        }

        foreach ($this->block_data as $key=>$values) {
            $images = $this->get_images($values);
            if (count($images) > 0) {
                $this->processor->cloneBlock($key, count($values), true, true);
                foreach ($images as $fieldName => $imagePath) {
                    if (isset($imagePath)) {
                        $this->processor->setImageValue($fieldName, ['path' => $imagePath, 'height' => '4in', 'width' => '5in']);
                    }else{
                        $mapFieldName = preg_replace('/#[0-9]+$/', '', $fieldName);
                        $this->processor->setValue($fieldName . $map[$mapFieldName] , null);
                    }
                }
                foreach ($values as $i => $rec) {
                    foreach ($rec as $fieldName => $value) {
                        $fieldName = sprintf("$fieldName#%d", $i + 1);
                        $this->processor->setValue($fieldName, $value);
                    }
                }
            }else{
                $this->processor->cloneBlock($key, count($values), true, false, $values);
            }

        }

        // Must do after detail to avoid duplicated field name of main and detail
        foreach ($this->main_data as $rec) {
            foreach ($rec as $key=>$value) {
                $imagePath = $this->get_image_path($value);
                if (isset($imagePath)) {
                    if (file_exists($imagePath)) {
                        $this->processor->setImageValue($key, $imagePath);
                    }else{
                        $this->processor->setValue($key, null);
                    }
                }else{
                    $this->processor->setValue($key, $value);
                }
            }
        }
    }

    private function get_images(&$values): array {
        $images = [];
        foreach ($values as $i => &$recRef) {
            foreach ($recRef as $fieldName => $value) {
                $imagePath = $this->get_image_path($value);
                if (isset($imagePath)) {
                    unset($recRef[$fieldName]);
                    if ($imagePath == '' || $imagePath=='null' || !file_exists($imagePath)) {
                        $images[sprintf("$fieldName#%d", $i + 1)] = null;
                    }else {
                        $images[sprintf("$fieldName#%d", $i + 1)] = $imagePath;
                    }
                }
            }
        }
        return $images;
    }

    private function get_image_path($value): ?string {
        if (strpos($value, "#image#")===0){
            return substr($value,7);
        }
        return null;
    }
    /**
     * @throws CopyFileException
     * @throws Exception
     * @throws CreateTemporaryFileException
     */
    public function output_word($filename=null): DownloadObject
    {
        if (!isset($this->processor)) {
            $this->create_word();
        }
        $tmp_filename = $this->processor->save();
        $this->word = IOFactory::load($tmp_filename);
        $writer = IOFactory::createWriter($this->word);
        ob_start();
        $writer->save('php://output');
        if (!isset($filename)){
            $filename = $this->get_output_file_name();
        }
        unlink($tmp_filename);
        return new DownloadObject($filename, ob_get_clean());
    }


    public function save_word($filepath){
        if (!isset($this->processor)) {
            try {
                $this->create_word();
                $this->processor->saveAs($filepath);
            } catch (CopyFileException|CreateTemporaryFileException $e) {

            }
        }
    }

    /**
     * @throws CopyFileException
     * @throws Exception
     * @throws CreateTemporaryFileException
     */
    public function save_pdf($filepath, $wordPath = null){
        Settings::setPdfRendererName(Settings::PDF_RENDERER_MPDF);
        Settings::setPdfRendererPath(__DIR__ . '/../../vendor/mpdf/mpdf');
        if (!isset($this->processor)) {
            $this->create_word();
        }
        if (!isset($wordPath)) {
            $tmp_filename = $this->processor->save();
        }else{
            $tmp_filename = $wordPath;
        }
        $this->word = IOFactory::load($tmp_filename);
        $writer = IOFactory::createWriter($this->word, 'PDF');
        $writer->save($filepath);
        if (!isset($wordPath)) {
            unlink($tmp_filename);
        }
    }

    /**
     * @throws CopyFileException
     * @throws Exception
     * @throws CreateTemporaryFileException
     */
    public function output_pdf($filename=null): DownloadObject
    {
        Settings::setPdfRendererName(Settings::PDF_RENDERER_MPDF);
        Settings::setPdfRendererPath(__DIR__ . '/../../vendor/mpdf/mpdf');

        if (!isset($this->processor)) {
            $this->create_word();
        }
        $tmp_filename = $this->processor->save();
        $this->word = IOFactory::load($tmp_filename);
        $writer = IOFactory::createWriter($this->word, 'PDF');
        ob_start();
        $writer->save('php://output');
        if (!isset($filename)){
            $filename = "report.pdf";
        }
        unlink($tmp_filename);
        return new DownloadObject($filename, ob_get_clean());
    }

    protected function get_temp_filename(): string
    {
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
        return __DIR__ . DIRECTORY_SEPARATOR .'tmp_' . $d->format("Ymd_His.u");
    }
}
