<?php

namespace Sannomiya\Util;

class LinQ
{
    private array $data;

    private array $lastDuplicatedItems = [];


    public function __construct(&$data)
    {
        $this->data = &$data;
    }

    public static function getInstance(&$data): LinQ
    {
        return new LinQ($data);
    }

    public static function remove(array &$array, $item) {
        $index = array_search($item, $array);
        if ($index !== false) {
            unset($array[$index]);
            $array = array_values($array);
            // array_splice($array, $index, 1);
        }
    }
    public static function replace(array &$array, $item, $newItem) {
        $index = array_search($item, $array);
        if ($index !== false) {
            $array[$index] = $newItem;
        }
    }

    public function any($function, ... $params): bool {
        foreach ($this->data as $item) {
            if ($function($item, ... $params)){
                return true;
            }
        }
        return false;
    }
    public function where($function, ... $params): LinQ {

        $data = [];
        foreach ($this->data as &$item) {
            if ($function($item, ... $params)){

                $data[] = &$item;
            }
        }
        return new LinQ($data);
    }

    public function distinct(): LinQ {
        $data = [];
        foreach ($this->data as &$item) {
            if (!in_array($item, $data)) {
                $data[] = &$item;
            }
        }
        return new LinQ($data);
    }

    public function deDuplicated($function, $takeFirst = true): LinQ {
        $data = [];
        $this->lastDuplicatedItems = [];
        foreach ($this->data as $item) {
            $found = false;
            for($i=0; $i<count($data); $i++) {
                if ($function($item, $data[$i])) {
                    $found = true;
                    $this->lastDuplicatedItems[] = $item;
                    if (!$takeFirst) {
                        $data[$i] = $item;
                    }
                    break;
                }
            }
            if (!$found) {
                $data[] = $item;
            }
        }
        return new LinQ($data);
    }
    public function fastDeDuplicated($function, $takeFirst = true): LinQ {
        $keys = [];
        $this->lastDuplicatedItems = [];
        foreach ($this->data as $item) {
            $key = $function($item);
            if (!isset($keys[$key])) {
                $keys[$key] = $item;
            }else{
                $this->lastDuplicatedItems[] = $item;
                if (!$takeFirst) {
                    unset($keys[$key]);
                    $keys[$key] = $item;
                }
            }
        }
        $data = array_values($keys);
        return new LinQ($data);
    }

    public function sort($function): LinQ {
        usort($this->data, $function);
        return $this;
    }

    public function select($function): LinQ {
        $data = [];
        foreach ($this->data as $item) {
            $data[] = $function($item);
        }
        return new LinQ($data);
    }

    public function groupBy($function): LinQ {
        $data = [];
        foreach ($this->data as $item) {
            $key = $function($item);
            if (!isset($data[$key])) {
                $data[$key] = new LinQGroup($key);
            }
            $data[$key]->data[] = $item;
        }
        $data = array_values($data);
        return new LinQ($data);
    }

    public function &data(): array {
        return $this->data;
    }

    public function &first() {
        $ret = null;
        if (count($this->data) > 0) {
            $ret = &$this->data()[0];
        }
        return $ret;
    }

    public function &last() {
        $ret = null;
        if (count($this->data) > 0) {
            $ret = &$this->data()[count($this->data) - 1];
        }
        return $ret;
    }

    public function join($separator=","): string {
        return implode($separator, $this->data);
    }

    public function count(): int
    {
        return count($this->data);
    }

    public function get(int $idx)
    {
        if ($idx < $this->count()) {
            return $this->data[$idx];
        }else{
            return null;
        }
    }

    /**
     * @return array
     */
    public function getLastDuplicatedItems(): array
    {
        return $this->lastDuplicatedItems;
    }
}
