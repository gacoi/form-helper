<?php

namespace Sannomiya\Util;

use DateTimeZone;
use Exception;


    /**
     * 日本語対応DateTime拡張クラス
     *
     * 元号、曜日などの日本語表記に対応したDateTime拡張クラス
     *
     * @version 1.0.1
     * @author chiyoyo
     * @caution PHP5.2以上
     */
class DateTimeJP extends \DateTime{
    // フォーマット定義追加
    const JP_DATE = 'JK年n月j日'; // 例:平成元年5月7日（明治5年以前は当時と異なる日付が出るので注意）
    const JP_TIME = 'Eg時i分s秒'; // 例:午後3時05分07秒

    const DEFAULT_TO_STRING_FORMAT = 'Y-m-d H:i:s'; // toString()で利用する表示フォーマット

    /**
     * 元号用設定
     * 日付はウィキペディアを参照しました
     *
     * @see http://ja.wikipedia.org/wiki/%E5%85%83%E5%8F%B7%E4%B8%80%E8%A6%A7_%28%E6%97%A5%E6%9C%AC%29 元号一覧 (日本)
     */
    private static array $gengoList = [
        ['name' => '令和', 'name_short' => 'R', 'timestamp' =>  1556636400],  // 2019-05-01,
        ['name' => '平成', 'name_short' => 'H', 'timestamp' =>  600188400],  // 1989-01-08,
        ['name' => '昭和', 'name_short' => 'S', 'timestamp' => -1357635600], // 1926-12-25'
        ['name' => '大正', 'name_short' => 'T', 'timestamp' => -1812186000], // 1912-07-30
        ['name' => '明治', 'name_short' => 'M', 'timestamp' => -3216790800], // 1868-01-25
    ];

    /** 日本語曜日設定 */
    private static array $weekJp = [
        0 => '日',
        1 => '月',
        2 => '火',
        3 => '水',
        4 => '木',
        5 => '金',
        6 => '土',
    ];
    /** 午前午後 */
    private static array $ampm = [
        'am' => '午前',
        'pm' => '午後',
    ];

    /**
     * 文字列に変換された際に返却するもの
     *
     * @return string
     */
    public function __toString()
    {
        return $this->format(self::DEFAULT_TO_STRING_FORMAT);
    }

    /**
     * 和暦などを追加したformatメソッド
     *
     * 追加した記号
     * J : 元号
     * b : 元号略称
     * K : 和暦年(1年を元年と表記)
     * k : 和暦年
     * x : 日本語曜日(0:日-6:土)
     * E : 午前午後
     *
     * @param string $format DateTime::formatに準ずるformat文字列
     * @return string
     */
    public function format($format): string
    {
        // 和暦関連のオプションがある場合は和暦取得
        $gengo = array();
        if (preg_match('/(?<!\\\)[JbKk]/', $format)) {
            foreach (self::$gengoList as $g) {
                if ($g['timestamp'] <= $this->getTimestamp()) {
                    $gengo = $g;
                    break;
                }
            }
            // 元号が取得できない場合はException
            if (empty($gengo)) {
                throw new Exception('Can not be converted to a timestamp : '.$this->getTimestamp());
            }
        }

        // J : 元号
        if ($this->isCharactor('J', $format)) {
            $format = $this->replaceCharactor('J', $gengo['name'], $format);
        }

        // b : 元号略称
        if ($this->isCharactor('b', $format)) {
            $format = preg_replace('/b/', '¥¥' . $gengo['name_short'], $format);
        }

        // K : 和暦用年(元年表示)
        if ($this->isCharactor('K', $format)) {
            $year = date('Y', $this->getTimestamp()) - date('Y', $gengo['timestamp']) + 1;
            $year = $year == 1 ? '元' : $year;
            $format = $this->replaceCharactor('K', $year, $format);
        }

        // k : 和暦用年
        if ($this->isCharactor('k', $format)) {
            $year = date('Y', $this->getTimestamp()) - date('Y', $gengo['timestamp']) + 1;
            $format = $this->replaceCharactor('k', $year, $format);
        }

        // x : 日本語曜日
        if ($this->isCharactor('x', $format)) {
            $w = date('w', $this->getTimestamp());
            $format = $this->replaceCharactor('x', self::$weekJp[$w], $format);
        }

        // 午前午後
        if ($this->isCharactor('E', $format)) {
            $a = date('a', $this->getTimestamp());
            $value = isset(self::$ampm[$a]) ? self::$ampm[$a] : '';
            $format = $this->replaceCharactor('E', $value, $format);
        }

        return parent::format($format);
    }

    /**
     * 指定した文字があるかどうか調べる（エスケープされているものは除外）
     * @param string $char 検索する文字
     * @param string $string 検索される文字列
     * @return boolean
     */
    private function isCharactor($char, $string): bool
    {
        return preg_match('/(?<!\\\)'.$char.'/', $string);
    }

    /**
     * 指定した文字を置換する(エスケープされていないもののみ)
     * @param string $char 置換される文字
     * @param string $replace 置換する文字列
     * @param string $string 元の文字列
     * @return string 置換した文字列
     */
    private function replaceCharactor($char, $replace, $string): string
    {
        // エスケープされていないもののみ置換
        $string = preg_replace('/(?<!\\\)'.$char.'/', '${1}'.$replace, $string);
        // エスケープ文字を削除
        $string = preg_replace('/\\\\'.$char.'/', $char, $string);
        return $string;
    }

    public static function createFromFormat($format, $datetime, DateTimeZone $timezone = null): \DateTime|bool
    {
        $map = [];
        $i = 0;

        // Replace pattern
        $newFormat = preg_replace_callback('/(.)/', function ($m) use (&$map, &$i){
            $i++;
            $map[$i] = $m[1];
            return match ($m[1]) {
                'J' => '([^0-9A-Za-z][^0-9A-Za-z])',
                'b' => '([RHSTMrhstm])',
                'K', 'k', 'Y', 'y', 'm', 'd', 'H', 'h', 'i', 's' => '([0-9]+)',
                default =>  "(\\" . $m[1] . ")"
            };
        }, $format);

        // Check if it matches
        if (preg_match("/^$newFormat$/", $datetime, $m) <= 0) {
            return false;
        }
        $newDatetime = $datetime;
        $ynFull = null;
        $ynShort = null;
        $iYn = null;
        $iK = null;
        $k = null;
        for ($i=1;$i<count($m);$i++) {
            switch ($map[$i]) {
                case 'b':
                    $iYn = $i;
                    $ynShort = $m[$i];
                    break;
                case 'J':
                    $iYn = $i;
                    $ynFull = $m[$i];
                    break;
                case 'K':
                case 'k':
                    $iK = $i;
                    $k = $m[$i];
                    break;
            }
        }

        if (!isset($iYn) && !isset($iK)) {
            return parent::createFromFormat($format, $datetime, $timezone);
        }


        // Process
        if (!isset($iYn) || !isset($iK)) {
            return false;
        }

        $pYn = 0;
        $pK = 0;

        for ($i = 1; $i < $iYn; $i++) {
            $pYn += mb_strlen($m[$i]);
        }
        for ($i = 1; $i < $iK; $i++) {
            $pK += mb_strlen($m[$i]);
        }

        // Find year
        $y = null;
        foreach (static::$gengoList as $rec) {
            if ($rec['name'] == $ynFull || strtolower($rec['name_short']) == strtolower($ynShort)) {
                $startTime =  $rec['timestamp'];
                $y = date('Y', $startTime) * 1 + ($k-1);
            }
        }

        // Replace
        if (!isset($y)) {
            return false;
        }

        $yn = $ynFull ?? $ynShort;
        if ($iK > $iYn) {
            $newDatetime = mb_substr($newDatetime, 0, $pK) . $y . mb_substr($newDatetime, $pK + mb_strlen($k));
            $newDatetime = mb_substr($newDatetime, 0, $pYn) . mb_substr($newDatetime, $pYn + mb_strlen($yn));
        } else {
            $newDatetime = mb_substr($newDatetime, 0, $pYn) . mb_substr($newDatetime, $pYn + mb_strlen($yn));
            $newDatetime = mb_substr($newDatetime, 0, $pK) . $y . mb_substr($newDatetime, $pK + mb_strlen($k));
        }

        $newFormat2 = preg_replace_callback('/([JbKk])/', function ($m){
            return match ($m[1]) {
                'J', 'b' => '',
                'K', 'k' => 'Y',
                default => $m[1],
            };
        }, $format);

        return parent::createFromFormat($newFormat2, $newDatetime, $timezone);
    }

}
