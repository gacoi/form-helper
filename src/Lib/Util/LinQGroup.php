<?php

namespace Sannomiya\Util;

class LinQGroup
{
    public $key;
    public ?array $data;
    public function __construct($key) {
        $this->key = $key;
        $this->data = [];
    }
}
